package ru.khalitov.weatherapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;


import ru.khalitov.weatherapp.model.MyCity;

public class BitmapImgActivity extends AppCompatActivity {
    /**@param shareImg, здесь отображаем погоду с картинкой на текущий день с WeatherActivity
     * @param bitmapShare, сюда ложим объект битмап, который содержит ViewLayout, картинку и текст
     * @param bitmapArray, принимаем маасив байтов через намерение, который содержит Bitmap
     */
    ImageView shareImg;
    Bitmap bitmapShare;
    byte[] bitmapArray;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitm_img);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(MyCity.myCity);

        shareImg = findViewById(R.id.share_image);

        Intent intent = getIntent();
        bitmapArray = intent.getByteArrayExtra(WeatherActivity.EXTRA_BIT);
        bitmapShare = getBitmapFromByteArray(bitmapArray);

        shareImg.setImageBitmap(bitmapShare);
    }
    /* метод конвертирует байты в оъект Bitmap
     */
    private Bitmap getBitmapFromByteArray(byte[] bitmap) {
        return BitmapFactory.decodeByteArray(bitmap , 0, bitmap.length);
    }
}
