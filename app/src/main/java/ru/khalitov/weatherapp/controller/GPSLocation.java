package ru.khalitov.weatherapp.controller;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

/**
 * Created by almaz-h on 09.03.2020
 */
public class GPSLocation implements LocationListener {
    /** класс для поиска координат пользователя, используем LocationManager и
     * запрашиваем местоположение по функционалу телефона GPS координаты.
     * Метод getLocation() вызываем в главной Activity, после нажатия на кнопку геоданных
     */
    Context context;

    public GPSLocation(Context c) {
        super();
        this.context = c;
    }

    public Location getLocation(){
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)!=
                PackageManager.PERMISSION_GRANTED){
            Toast.makeText(context,"Разрешение не предоставлено",Toast.LENGTH_LONG).show();
            return null;
        }
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 6000, 0, this);
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                return location;
            } else {
                Toast.makeText(context, "Включите GPS на телефоне или введите город в ручную", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
