package ru.khalitov.weatherapp.controller;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.khalitov.weatherapp.R;
import ru.khalitov.weatherapp.model.WeatherDay;
import ru.khalitov.weatherapp.model.WeatherForecast;

public class WeatherAPI {
    /**
     * @param ID, ключ, полученный при регистрации на openweathermap
     * @param URL, адрес запроса
     */

    private final String ID = "288659209c7c707e25a571b12cabafb3";
    private static final String URL = "https://api.openweathermap.org/";

    public String getID() {
        return ID;
    }

    public interface Api {
        @GET("data/2.5/weather")
        Call<WeatherDay> getToday(@Query("q") String myCity, @Query("units") String units,
                @Query("appid") String appid
        );

        @GET("data/2.5/forecast")
        Call<WeatherForecast> getForecast(@Query("q") String myCity, @Query("units") String units,
                @Query("appid") String appid
        );
    }
    private static Retrofit retrofit;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
