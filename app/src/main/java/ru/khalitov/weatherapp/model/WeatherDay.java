package ru.khalitov.weatherapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.List;

public class WeatherDay {
    /**
     * Основной класс модели данных "погода", отсюда через геттеры получаем
     * температуру, подгружаем иконки температуры и остальные погодные условия
     */
    @SerializedName("dt")
    private long dateTime;

    @SerializedName("main")
    private WeatherMain main;

    @SerializedName("weather")
    private List<WeatherDescriptions> descriptions;

    public WeatherDay(WeatherMain main, List<WeatherDescriptions> descriptions) {
        this.main = main;
        this.descriptions = descriptions;
    }

    public Calendar getDate() {
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(dateTime * 1000);
        return date;
    }

    public String getTemp() { return main.temp + "\u00B0"; }

    public String getFeels() { return "Ощущается: "+main.feels_like+ "\u00B0"; }

    public String getHumidity() { return "Влажность: "+main.humidity+"%"; }

    public String getIconUrl() {
        return "http://openweathermap.org/img/w/" + descriptions.get(0).icon + ".png";
    }
}
