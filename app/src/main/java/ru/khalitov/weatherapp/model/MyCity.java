package ru.khalitov.weatherapp.model;

public class MyCity {
    /**
     * @param myCity, сохраняем полученный с главной активиности город,
     * для дальнейшей работы с ним
     */
    public static String myCity;

    public MyCity() {
    }


    public String getMyCity() {
        return myCity;
    }

}
