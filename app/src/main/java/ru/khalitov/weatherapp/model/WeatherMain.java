package ru.khalitov.weatherapp.model;

class WeatherMain {
    /** Все параметры получаем из ресурса "open_weather_maps_URL"
     * @param temp - текущая температура
     * @param feels_like - температура по ощущениям
     * @param humidity - влажность
     *                 геттеры в основном классе модели данных "WeatherDay"
     */
    double temp;
    double feels_like;
    int humidity;
}
