package ru.khalitov.weatherapp.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class WeatherForecast {
    /**
     * Класс с подробным отображением погоды на несколько дней
     */

    @SerializedName("list")
    private List<WeatherDay> items;

    public WeatherForecast(List<WeatherDay> items) {
        this.items = items;
    }

    public List<WeatherDay> getItems() {
        return items;
    }
}
