package ru.khalitov.weatherapp.model;

import android.app.Activity;
import android.content.SharedPreferences;

import ru.khalitov.weatherapp.MainActivity;

public class CityPreference {
    /** @param CITY, константа, Preferences работает с данными в паре ключ-значение
     */
    private final String CITY = "City";

    private SharedPreferences prefs;

    /**
     * Конструктор, класс вызывается в главной Активности
     */
    public CityPreference(Activity activity){
        prefs = activity.getPreferences(Activity.MODE_PRIVATE);
    }

    /**
     * @param city, передаем введенное пользователем текст в метод, сохраняем данные через
     *              SharedPreferences
     */
    public void setCity(String city){
        prefs.edit().putString(CITY, city).apply();
    }

}
