package ru.khalitov.weatherapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ru.khalitov.weatherapp.controller.GPSLocation;
import ru.khalitov.weatherapp.model.CityPreference;
import ru.khalitov.weatherapp.view.EdTxtView;
import ru.khalitov.weatherapp.view.GeoCoderLocation;
import ru.khalitov.weatherapp.view.MyAnimation;

public class MainActivity extends AppCompatActivity {
    //-------------------------------------------------------------------//
    /**
     * @param currentLatitude,currentLongitude , значения долготы и широты
     */
    private double currentLatitude;
    private double currentLongitude;
    /**
     * @param EXTRA_STRING, работа с методом класса Intent,
     *                      первый параметр метода putExtra()
     */
    public static final String EXTRA_STRING = "EXTRA_STRING";
    //-------------------------------------------------------------------//
    /**
     * @param formattedDate
     * сохраняет данные о дате в строке, для передачи объекту TextView
     * для вывода на экран пользователя
     */
    String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    public final String TAG = this.getClass().getSimpleName();

    TextView textDate;
    /**
     * @param textCity, поле куда пользователь вводит свой город
     * @param cityMyLoc, сюда записывается введенное пользователем, или найденное по геолокации местоположение
     * @param cityInp, введенный пользователем город в поле EditText textCity
     */
    EditText textCity;
    Button buttonGeo;   //находит координаты города
    TextView textGeo;
    Button buttonNext;
    private String cityMyLoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermission();

        textDate = findViewById(R.id.text_Date);
        textDate.setText(String.format(" %s %s", getString(R.string.in_dspl_date), formattedDate));
        textCity = findViewById(R.id.text_city);
        EdTxtView.focusAndShKey(textCity, this);

        buttonGeo = findViewById(R.id.btn_geo);
        textGeo = findViewById(R.id.text_geo);
        buttonNext = findViewById(R.id.button_next);
        /*
         * @param animAlpha, инициализируем ресурс с настройкой анимации нажатия кнопки
         */
        final MyAnimation myAnimation = new MyAnimation(getApplicationContext());
        //-------------------------------------------------------------------//
        textCity.setOnKeyListener(new View.OnKeyListener() {
                                      public boolean onKey(View v, int keyCode, KeyEvent event) {
                                          if (event.getAction() == KeyEvent.ACTION_DOWN &&
                                                  keyCode == KeyEvent.KEYCODE_ENTER) {
                                              /* сохраняем текст, после нажатия Enter в переменную,
                                              так же информация передается методу changeCity,
                                              который хранит имя города с использованием класса CityPreference
                                               */
                                              cityMyLoc = changeCity(textCity.getText().toString());
                                              Log.i(TAG,"Город: "+ cityMyLoc);
                                              return true;
                                          }
                                          return false;
                                      }
                                  });
        /** api, установлен для отображения города на русском языке в методе getStringInLocation.
         *  По нажаитию на кнопку геолокации(buttonGeo) вызываем метод поиска координат по GPS(getLocation)
         *  из класса GPSLocation. Записываем полученные координаты в переменные double и передаем их в метод
         *  конвертации координат в наименование города, значение сохраняем в строку
         * @param cityMyLoc, которую передаем во вторую Activity для получения данных о погоде.
         */
        buttonGeo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                v.startAnimation(myAnimation.getAnimation());
                GPSLocation gpsLocation = new GPSLocation(getApplicationContext());
                Location loc = gpsLocation.getLocation();
                if(loc!=null){
                    currentLatitude = loc.getLatitude();
                    currentLongitude = loc.getLongitude();
                    Log.i(TAG, currentLatitude + "\n" + currentLongitude);
                    GeoCoderLocation geo = new GeoCoderLocation(getApplicationContext());
                    cityMyLoc = geo.getStringInLocation(currentLatitude,currentLongitude);
                    Log.i(TAG,"Город: "+ cityMyLoc);
                    Toast.makeText(getApplicationContext(),"Ваш город: "+cityMyLoc+"," +
                            "\nнажмите кнопку 'Далее'",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(),"Локация не обнаружена",Toast.LENGTH_LONG).show();
                }
            }
        });
        //-------------------------------------------------------------------//
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(myAnimation.getAnimation());
                if(cityMyLoc != null){
                    Intent intent = new Intent(getApplicationContext(), WeatherActivity.class);
                    intent.putExtra(EXTRA_STRING, cityMyLoc);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), R.string.dont_ch_city,Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    /* метод requestPermission() запрашивает разрешение на доступ к функционалу геолокация
     * на телефоне пользователя
     */
    private void requestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},123);
    }
    /**
     * @param city, введенный пользователем с клавиатуры город
     *              метод используется в слушателе объекта EitText(textCity)
     */
    public String changeCity(String city) {
        new CityPreference(this).setCity(city);
        return city;
    }
}
//-------------------------------------------------------------------//





