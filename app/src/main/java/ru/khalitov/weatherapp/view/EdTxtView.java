package ru.khalitov.weatherapp.view;


import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class EdTxtView {

    /**
     * Метод , для того чтобы в объекте TextView всплывала клваиатура
     * @param et передаваемая ссылка на объект EditText
     * @param context Context
     */
    public static void focusAndShKey(final EditText et, final Context context) {
        et.requestFocus();
        et.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
            }
        }, 300);
    }
}
