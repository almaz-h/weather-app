package ru.khalitov.weatherapp.view;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import ru.khalitov.weatherapp.R;

public class MyAnimation {
    /**
     * класс с вызовом настроек для отображения анимации, при нажатии на кнопки
     */
    private Animation animation;
    public MyAnimation(Context context)
    {
        animation = AnimationUtils.loadAnimation(context, R.anim.alpha);
    }

    public Animation getAnimation() {
        return animation;
    }
}
