package ru.khalitov.weatherapp.view;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.android.gms.common.internal.Objects;

import java.util.List;
import java.util.Locale;

/**
 * Created by almaz-h on 09.03.2020
 */
public class GeoCoderLocation {
    /** класс для конвертации широты и долготы в строку, передаем найденные координаты
     * в метод getStringInLocation. Метод getLocality() возвращает значение "Город".
     */
    Context mContext;

    public GeoCoderLocation(Context c) {
        this.mContext = c;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public String getStringInLocation(double lat, double lon){
     String adr = "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(mContext, Locale.forLanguageTag("ru"));
        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses!=null && addresses.size()>0){
                adr = addresses.get(0).getLocality();
            }else {
                Toast.makeText(mContext,"Адрес не найден",Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return adr;
    }
}
