package ru.khalitov.weatherapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Locale;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.khalitov.weatherapp.controller.WeatherAPI;
import ru.khalitov.weatherapp.model.MyCity;
import ru.khalitov.weatherapp.model.WeatherDay;
import ru.khalitov.weatherapp.model.WeatherForecast;
import ru.khalitov.weatherapp.view.MyAnimation;

public class WeatherActivity extends AppCompatActivity implements Serializable {

    MyCity myCity = new MyCity();
    public static final String EXTRA_BIT = "MY_BITMAP_LAYOUT";
    /**
     * @param tvTemp, вьюшка с данными о температуре сегодня
     * @param tvImage, картинка иконка погоды, подгружается с библиотеки Glide
     *                 остальный описаны ниже
     */
    TextView tvDay;
    ImageView ivIcon;
    TextView tvTemp;
    ImageView tvImage;
    WeatherAPI.Api api;
    /**
     * @param tvFeels, сюда сохраняем темпаратуру по "ощущениям", для вывода на экран
     * @param tvHumidity, влажность,
     *                    данные берем через геттеры из дата-класса WeatherDay
     */
    TextView tvFeels;
    TextView tvHumidity;
    LinearLayout llForecast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        /*
         * Используем собственный Toolbar в данном Activity, используем поддержку
         * androidx.appcompat.widget.Toolbar
         */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        MyCity.myCity = intent.getStringExtra(MainActivity.EXTRA_STRING);
        /* Устанавливаем заголовок в Toolbar-название выбранного города
         */
        getSupportActionBar().setTitle(myCity.getMyCity());

        tvTemp = findViewById(R.id.tv_temp);
        tvImage = findViewById(R.id.iv_image);
        tvFeels = findViewById(R.id.tv_feels);
        tvHumidity = findViewById(R.id.tv_humidity);
        llForecast = findViewById(R.id.llForecast);
        /* инициализируем API и вызываем метод для поиска и отображения в макете данных о погоде
         */
        api = WeatherAPI.getClient().create(WeatherAPI.Api.class);
        getWeather(getCurrentFocus());

    }
    public void getWeather(View v) {
        /*
         * @param units, система исчесления температуры, используется как параметр, в методе интерфейса API,
         * для запроса данных по городу, метрической системе и ключу
         * @param key, ключ(ID), сгенерированный при регистрации в сервисе openweathermap
         */
        String units = "metric";
        String key = new WeatherAPI().getID();
        // делаем вызов для получения текущей температуры
        Call<WeatherDay> callToday = api.getToday(myCity.getMyCity(), units, key);
        callToday.enqueue(new Callback<WeatherDay>() {
            @Override
            public void onResponse(Call<WeatherDay> call, Response<WeatherDay> response) {
                WeatherDay data = response.body();
                if (response.isSuccessful()) {
                    tvTemp.setText(data.getTemp());
                    Glide.with(getApplicationContext()).load(data.getIconUrl()).into(tvImage);
                    tvFeels.setText(data.getFeels());
                    tvHumidity.setText(data.getHumidity());
                }
            }
            @Override
            public void onFailure(Call<WeatherDay> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ошибка подключения к API", Toast.LENGTH_LONG).show();
            }
        });
        //погода на будущее
        Call<WeatherForecast> callForecast = api.getForecast(myCity.getMyCity(), units, key);
        callForecast.enqueue(new Callback<WeatherForecast>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<WeatherForecast> call, Response<WeatherForecast> response) {
                WeatherForecast data = response.body();
                if (response.isSuccessful()) {
                    //выводим день недели и время в tvDay
                    SimpleDateFormat formatDayOfWeek = new SimpleDateFormat("E\nHH:mm", Locale.forLanguageTag("ru"));
                    //верстка выводимых данных
                    LayoutParams paramsTextView = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                    LayoutParams paramsImageView = new LinearLayout.LayoutParams(
                            LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                    //отступы для красивого вывода
                    int marginLeft = 50;
                    int marginRight = 50;
                    LayoutParams paramsLinearLayout = new LayoutParams(LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT);
                    paramsLinearLayout.setMargins(marginLeft, 0, marginRight, 0);

                    llForecast.removeAllViews();

                    for (WeatherDay day : data.getItems()) {
                        // создаем LinerLayout, там будут вьюшки для вывода в Макете Активности
                        LinearLayout childLayout = new LinearLayout(getApplicationContext());
                        childLayout.setLayoutParams(paramsLinearLayout);
                        childLayout.setOrientation(LinearLayout.VERTICAL);
                        /*
                         * @param tvDay, сохраняем день недели со списка WeatherForecast
                         */
                        tvDay = new TextView(getApplicationContext());
                        String dayOfWeek = formatDayOfWeek.format(day.getDate().getTime());
                        tvDay.setText(dayOfWeek);
                        tvDay.setLayoutParams(paramsTextView);
                        childLayout.addView(tvDay);
                        /*
                         * выводим ниже текста с текущим днем, иконку погоды tvIcon,
                         * подтягиваем из библиотеки Glide
                         */
                        ivIcon = new ImageView(getApplicationContext());
                        ivIcon.setLayoutParams(paramsImageView);
                        Glide.with(getApplicationContext()).load(day.getIconUrl()).into(ivIcon);
                        childLayout.addView(ivIcon);
                        /*
                         * под иконкой ivIcon показываем температуру, tvTemp
                         */
                        tvTemp = new TextView(getApplicationContext());
                        tvTemp.setText(day.getTemp());
                        tvTemp.setLayoutParams(paramsTextView);
                        childLayout.addView(tvTemp);

                        llForecast.addView(childLayout);
                    }
                }
            }
            @Override
            public void onFailure(Call<WeatherForecast> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ошибка подключения к API", Toast.LENGTH_LONG).show();
            }
        });
    }
    /* действие при нажатии на кнопку Share, передаем  изображение погоды в третью Activity,
     * используем методы getViewToBitmap, getByteArrayFromBitmap.
     */
    public void putByteBitmap(View w) {
        MyAnimation animation = new MyAnimation(getApplicationContext());
        w.startAnimation(animation.getAnimation());
        LinearLayout linearLayout = findViewById(R.id.llToday);

        Bitmap bitmap = getViewToBitmap(linearLayout);
        byte[] array = getByteArrayFromBitmap(bitmap);
        Intent intent = new Intent(getApplicationContext(), BitmapImgActivity.class);
        intent.putExtra(EXTRA_BIT, array);
        startActivity(intent);
    }
    /**
     * @param view, параметром будет выступать View LinerLayout с данными о погоде на текущий день
     * @return bitmap, View в виде объекта Bitmap
     */
    private Bitmap getViewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
    /**
     * @param bitmap, передаем Bitmap и конвертируем в массив байт, для передачи данных
     *                через намерение (Intent)
     * @return byte[]array, возвращает массив байт
     */
    private byte[] getByteArrayFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
        return bos.toByteArray();
    }
}


